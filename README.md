![cheshirecat.jpg](https://bitbucket.org/repo/ode949/images/2848830820-cheshirecat.jpg)

ALICE'S ADVENTURES IN WONDERLAND
================================
By Lewis Carroll

THE MILLENNIUM FULCRUM EDITION 3.0

CONTENTS:
--------- 

* CHAPTER I.
    * Down the Rabbit-Hole
* CHAPTER II.
    * The Pool of Tears
* CHAPTER III.
    * A Caucus-Race and a Long Tale
* CHAPTER IV.
    * The Rabbit Sends in a Little Bill
* CHAPTER V.
    * Advice from a Caterpillar
* CHAPTER VI.
    * Pig and Pepper
* CHAPTER VII.
    * A Mad Tea-Party
* CHAPTER VIII.
    * The Queen's Croquet-Ground
* CHAPTER IX.
    * The Mock Turtle's Story
* CHAPTER X.
    * The Lobster Quadrille
* CHAPTER XI.
    * Who Stole the Tarts?
* CHAPTER XII.
    * Alice's Evidence

DETAILS:
--------

Project Gutenberg's Alice's Adventures in Wonderland, by Lewis Carroll

This eBook is for the use of anyone anywhere at no cost and with
almost no restrictions whatsoever.  You may copy it, give it away or
re-use it under the terms of the Project Gutenberg License included
with this eBook or online at www.gutenberg.org

Title: Alice's Adventures in Wonderland

Author: Lewis Carroll

Release Date: June 25, 2008 \[EBook #11\]
Last Updated: July 14, 2014

Language: English

Character set encoding: ASCII

Produced by Arthur DiBianca and David Widger

Updated editions will replace the previous one--the old editions
will be renamed.

Creating the works from public domain print editions means that no
one owns a United States copyright in these works, so the Foundation
(and you!) can copy and distribute it in the United States without
permission and without paying copyright royalties.  Special rules,
set forth in the General Terms of Use part of this license, apply to
copying and distributing Project Gutenberg-tm electronic works to
protect the PROJECT GUTENBERG-tm concept and trademark.  Project
Gutenberg is a registered trademark, and may not be used if you
charge for the eBooks, unless you receive specific permission.  If you
do not charge anything for copies of this eBook, complying with the
rules is very easy.  You may use this eBook for nearly any purpose
such as creation of derivative works, reports, performances and
research.  They may be modified and printed and given away--you may do
practically ANYTHING with public domain eBooks.  Redistribution is
subject to the trademark license, especially commercial
redistribution.

Read the full gutenberg license [here](http://gutenberg.org/license).

[Alice's Adventures in Wonderland on gutenberg.org](http://gutenberg.org/ebooks/11)